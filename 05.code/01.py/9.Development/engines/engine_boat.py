# import GPIO controls
import RPi.GPIO as GPIO
import time 

#lets cleanup the GPIO first, just incase
#GPIO.cleanup()

# set to use physical pin numbers (01-40, rather than name GPIO01 etc. = GPIO.BCM)
GPIO.setmode(GPIO.BOARD)

# define pins to use
fwPin = 16
rvPin = 18

# setup pins with GPIO
GPIO.setup(fwPin, GPIO.OUT)
GPIO.setup(rvPin, GPIO.OUT)

# now activate pin one for half a second seconds then off
GPIO.output(fwPin, GPIO.HIGH)
time.sleep(0.5)
GPIO.output(fwPin,GPIO.LOW)

# now activat pin two
GPIO.output(rvPin, GPIO.HIGH)
time.sleep(0.5)
GPIO.output(rvPin,GPIO.LOW) 

# and clear the GPIO
GPIO.cleanup()


# now try PWM for speed control
from RPIO import PWM
PWM.setup()

#initalise PWN with a DMA channel (0)
GPIO.setmode(GPIO.BOARD)
PWM.init_channel(0)

#pulse at width*granularuty 
#pulse at about quarter power (i thin its 0-2k)
PWM.add_channel_pulse(0, fwPin,0, 500)
time.sleep(2)
# I think 1999 is the max width, so 1500 should be about 3/4 power
PWM.add_channel_pulse(0,fwPin,0,1500)

#clear the DMA channel onth pin
PWM.clear_channel_gpio(0,fwPin)


# repeat low and high, but in reverse still using channel(0), second moter will need another DMA channel.
#PWM.add_channel_pulse(0,rvPin,0,0.25)
#time.sleep(2)
#PWM.add_channel_pulse(0,rvPin,0,0.75)
#PWM.clear_channel_gpio(0,rvPin)

