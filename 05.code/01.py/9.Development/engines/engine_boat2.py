# import GPIO controls
import RPi.GPIO as GPIO
from RPIO import PWM
import time 
engine_init_state = 0





def init_engine()
	global engine_init_state
	GPIO.setmode(GPIO.BOARD)
	PWM.setup()
	RfwPin = 22
	RrvPin = 18
	RpwmPin = 16

	LfwPin = 15
	LrvPin = 13
	LpwmPin = 11 

	# want to set this as an object engine.RfwPin etc engine.init_state   that can be called from anywhere.
	# not sure if this is done by making it a class
	
	GPIO.setup(RfwPin,GPIO.OUT)
	GPIO.setup(RrvPin,GPIO.OUT)
	PWM.init_channel(0)

	GPIO.setup(LfwPin,GPIO.OUT)
	GPIO.setup(LrvPin,GPIO.OUT)
	PWM.init_channel(1)
	
	engine_init_state=1
	# repeat for channel 1 and Lpins

def set_engine(l_speed, r_speed)
	global engine_init_state
	if engine_init_state != 1
		init_engine()

	if (r_speed > 3) or (r_speed == 0) or (r_speed < -3)
		GPIO.output(RfwPin,GPIO.LOW)
		GPIO.output(RrvPin,GPIO.LOW)
		PWM.add_channel_pulse(0,Rpwmpin,0,0)
	elif r_speed > 0
		GPIO.output(RfwPin,GPIO.HIGH)
		GPIO.output(RrvPin,GPIO.LOW)
		PWM.add_channel+pulse(0,RpwmPin,0,r_speed/3*1999)
	elif r_speed < 0
		GPIO.output(RfwPin,GPIO.LOW)
		GPIO.output(RrvPin,GPIO.HIGH)
		PWM.add_channel_pulse(0,RpwmPin,o,-1*r_speed/3*1999)


	if (l_speed > 3) or (l_speed == 0) or (l_speed < -3)
		GPIO.output(RfwPin,GPIO.LOW)
		GPIO.output(RrvPin,GPIO.LOW)
		PWM.add_channel_pulse(0,Rpwmpin,0,0)
	elif l_speed > 0
		GPIO.output(RfwPin,GPIO.HIGH)
		GPIO.output(RrvPin,GPIO.LOW)
		PWM.add_channel+pulse(0,RpwmPin,0,l_speed/3*1999)
	elif l_speed < 0
		GPIO.output(RfwPin,GPIO.LOW)
		GPIO.output(RrvPin,GPIO.HIGH)
		PWM.add_channel_pulse(0,RpwmPin,o,-1*l_speed/3*1999)
