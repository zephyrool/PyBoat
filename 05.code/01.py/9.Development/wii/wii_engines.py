
import time
import RPi.GPIO as GPIO
import cwiid
GPIO.setmode(GPIO.BOARD)

# notification leds pins setup
Blue_pin = 11
Red_pin = 13
LEDFlashCount =3
LEDFlashDelay =0.5
GPIO.setup(Blue_pin,GPIO.OUT)
GPIO.setup(Red_pin,GPIO.OUT)

# setup wee controller
ButtonDelay = 0.1
# turn on red led to notify user to connect (press 1+2 togehter
GPIO.output(Red_pin,GPIO.HIGH)
time.sleep(1)
try:
  wii=cwiid.Wiimote()
except RuntimeError:
  #flash red led if error
  flash(Red_pin,LEDFlashCount,LEDFlashDelay)
  quit()

# if connected then flash blue and turn off red led
flash(Blue_pin,LEDFlashCount,LEDFlashDelay)
GPIO.output(Red_pin,GPIO.HIGH)


#set up wii to report buttons and NUNCHUCKs
wii.rpt_mode = cwiid.RPT_BTN |cwiid.RPT_NUNCHUK

# Setup engine control pins parameters
R_FW_pin = 22
R_RV_pin = 18
R_PW_pin = 16

L_FW_pin = 15
L_RV_pin = 13
L_PW_pin = 11 

# defaults to use for speed control
PW_frequency = 100
FW_speed_default = 50
RV_speed_default = -30
increment_speed_default = 10
speed_max = 80

#setup GPIO outputs
GPIO.setup(R_FW_pin,GPIO.OUT)
GPIO.setup(R_RV_pin,GPIO.OUT)
GPIO.setup(R_PW_pin,GPIO.OUT)

GPIO.setup(L_FW_pin,GPIO.OUT)
GPIO.setup(L_RV_pin,GPIO.OUT)
GPIO.setup(L_PW_pin,GPIO.OUT)


# set up engine PWM, 100hz
L_engine = GPIO.PWM(L_PW_pin,PW_frequency)
R_engine = GPIO.PWM(R_PW_pin,PW_frequency)
L_engine.start(0)
R_engine.start(0)

L_speed = 0
R_speed = 0


# button reading
while True:
  buttons = wii.state['buttons']
 
  # If Plus and Minus buttons pressed
  # together then rumble and quit.
  if (buttons - cwiid.BTN_PLUS - cwiid.BTN_MINUS == 0):
            wii.rumble =1
             time.sleep(0.5)
           wii.rumble =0
            L_engine.stop()
            R_engine.stop()
            exit(wii)
    


            
  #button 1 for dead stop
  if [1]:
            L_speed = 0
            R_speed = 0
            set_engines(L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine)

  # dpad controls for basic controls
  if [down]:
            L_speed = RV_speed_default
            R_speed = RV_speed_default
            set_engines(L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine)
  if [up]:
            L_speed = FW_speed_default
            R_speed = FW_speed_default
            set_engines(L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine)
if [left]:
            L_speed = FW_speed_default
            R_speed = 0
            set_engines(L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine)
  if [right]:
            L_speed = 0
            R_speed = FW_speed_default
            set_engines(L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine)

  # trigger controls to increment and decrement speed on each side

  if [B]:
            R_speed = R_speed + increment_speed_default
            if R_speed >speed_max:
                        R_speed = speed_max
            set_engines(L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine)
  if [A]:
            R_speed = R_speed - increment_speed_default
            if R_speed < -speed_max:
                        R_speed = -speed_max
            set_engines(L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine) 
  if [Z]:
            L_speed = L_speed + increment_speed_default
            if L_speed >speed_max:
                        L_speed = speed_max
            set_engines(L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine)
  if [C]:
            L_speed = L_speed - increment_speed_default
            if L_speed < -speed_max:
                        L_speed = -speed_max
            set_engines(L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine)

# set speed of engines
def set_engines(L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine):
            set_direction(L_speed,L_FW_pin,L_RV_pin)
            set_direction(R_speed,R_FW_pin, R_RV_pin)
            set_speed (L_engine, L_speed)
            set_speed (R_engine, R_speed)
          #  set the direction using the fw and rv pins
            def set_direction(speed, FW_pin, RV_pin):
                        if speed = 0:
                                    GPIO.output(FW_pin,GPIO.LOW)
                                    GPIO.output(RV_pin,GPIO.LOW)
                        elif speed < 0:
                                    GPIO.output(FW_pin,GPIO.LOW)
                                    GPIO.output(RV_pin,GPIO.HIGH)
                        elif speed > 0 :
                                    GPIO.output(FW_pin,GPIO.HIGH)
                                    GPIO.output(RV_pin,GPIO.LOW)
            # set the speed or the pwm duty cycle
            def set_speed (engine, speed):
                        if speed < 0 :
                                    speed = -speed
                        if speed > 100:
                                    speed = 100
                        engine.ChangeDutyCycle(speed)

# flashing LED routine for notifications
def flash(pin,LEDFlashCount,LEDFlashDelay):
  for n in range(1,LEDFlashCount):
    GPIO.output(pin,GPIO.HIGH)
    time.sleep(LEDFlashDelay)
    GPIO.output(pin,GPIO.LOW)
    time.sleep(LEDFlashDelay)
