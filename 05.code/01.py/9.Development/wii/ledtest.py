import time
import RPi.GPIO as GPIO
import cwiid

GPIO.setmode(GPIO.BOARD)
# notification leds pins setup
Blue_pin = 11
Red_pin = 13
LEDFlashCount = 3
LEDFlashDelay = 0.5
GPIO.setup(Blue_pin,GPIO.OUT)
GPIO.setup(Red_pin,GPIO.OUT)

# flashing LED routine for notifications
def flash(pin,LEDFlashCount,LEDFlashDelay):
	for n in range(0, LEDFlashCount):
		GPIO.output(pin,GPIO.HIGH)
		time.sleep(LEDFlashDelay)
		GPIO.output(pin,GPIO.LOW)
		time.sleep(LEDFlashDelay)
		
#now flashthings
#GPIO.output(Blue_pin,GPIO.HIGH)
time.sleep(LEDFlashDelay)
flash(Red_pin,LEDFlashCount,LEDFlashDelay)
time.sleep(LEDFlashDelay)
#GPIO.output(Red_pin,GPIO.HIGH)
flash(Blue_pin,LEDFlashCount,LEDFlashDelay)
GPIO.cleanup()
exit(0)
