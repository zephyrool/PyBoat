import functools
import os
import random
import time
from piui import PiUi
import RPi.GPIO as GPIO

current_dir = os.path.dirname(os.path.abspath(__file__))

engine_init_state=0
l_status = 0
r_status = 0
status = object

class BoatPiUi(object):

    def __init__(self):
        self.title = None
        self.txt = None
        self.img = None
        self.ui = PiUi(img_dir=os.path.join(current_dir, 'imgs'))
        self.src = "sunset.png"

    def main_menu(self):
        global l_status
        global r_status
        global status 

        self.page = self.ui.new_ui_page(title="Boat control")
        self.page.add_element("hr")
        self.page.add_textbox("Current Engine Status", "p")
        self.page.add_textbox("LEFT | RIGHT", "p")
        status = self.page.add_textbox("L "+ str(l_status) + "  |  R " + str(r_status), "h2")

        self.page.add_element("hr")
          both_stop = self.page.add_button("Both Halt", self.onhaltclick)
        self.page.add_element("hr")
          self.page.add_textbox("Basic", "p")
          both_fw = self.page.add_button("LR FW", self.onf1click)
          self.page.add_element("br")
          both_rv = self.page.add_button("LR RV", self.onr1click)

        self.page.add_element("hr")
          self.page.add_textbox("Forward Control", "p")
          left_fw = self.page.add_button("L FW", self.onlf1click)
          right_fw = self.page.add_button("R FW", self.onrf1click)
          self.page.add_element('br')
        self.page.add_element('hr')
          self.page.add_textbox("Reverse Control", "p")
          left_rv = self.page.add_button("L RV", self.onlr1click)
          right_rv = self.page.add_button("R RV", self.onrr1click)
          self.page.add_element('br')
        self.page.add_element('hr')
          self.page.add_textbox("Fast Turn Control", "p")
          both_fr = self.page.add_button("L FW - R RV", self.onbfr1click)
          both_rf = self.page.add_button("L RV - R FW", self.onbrf1click)
          self.page.add_element('br')
        self.page.add_element('hr')
          shutdown = self.page.add_button("shutdown", self.onshutdownclick)
          self.page.add_textbox("End of controls","p")
        self.page.add_element('hr')
                                         
    def onhaltclick(self):
        self.setengines(0,0)
        
    def onf1click(self):
        self.setengines(1,1)
    def onr1click(self):
        self.setengines(-1,-1)
    
    def onlf1click(self):
        self.setengines(1,0)
    def onrf1click(self):
        self.setengines(0,1)
                    
    def onlr1click(self):
        self.setengines(-1,0)
    def onrr1click(self):
        self.setengines(0,-1)
        
    def onbfr1click(self):
        self.setengines(1,-1)
    def onbrf1click(self):
        self.setengines(-1,1)
        
    def onshutdownclick(self):
        global engine_init_state
        GPIO.cleanup()
        engine_init_state = 0
        
    def setengines(self, l_value, r_value):
        # this sets a status line to display on the ui, as well as calls setengines, which actually controls the motors
        global l_status
        global r_status
        global status
        # the 9 value is used to preserve the current stats of an engine (not used in binary)
        if r_value != 9:
            r_status = r_value
        if l_value != 9:
            l_status = l_value
            
        call setEngine(l_status,r_status)
        status.set_text("L "+ str(l_status) + "  |  R " + str(r_status))
            
    def main(self):
        self.main_menu()
        self.ui.done()  
    
def main():
  piui = BoatPiUi()
  piui.main()

if __name__ == '__main__':
main()

# motor control
def initEngine():
    global engine_init_state
    GPIO.cleanup() 
    GPIO.setmode(GPIO.BOARD)
    LfwPin = 16
    LrvPin = 18

    RfwPin = 12
    RrvPin = 12

    GPIO.setup(LfwPin, GPIO.OUT)
    GPIO.setup(LrvPin, GPIO.OUT)
    GPIO.setup(RfwPin, GPIO.OUT)
    GPIO.setup(RrvPin, GPIO.OUT)
    engine_init_state=1

def setEngine(left, right):
    global engine_init_state
	if engine_init_state != 1:
        call init_engine()

    if left == 1:
        GPIO.output(LfwPin, GPIO.HIGH)
        GPIO.output(LrvPin, GPIO.LOW)
    elif left == -1:
        GPIO.output(LfwPin, GPIO.LOW)
        GPIO.output(LrvPin, GPIO.HIGH)
    else:
        GPIO.output(LfwPin, GPIO.LOW)
        GPIO.output(LrvPin, GPIO.LOW)
    
    if right == 1:
        GPIO.output(RfwPin, GPIO.HIGH)
        GPIO.output(RrvPin, GPIO.LOW)
    elif right == -1:
        GPIO.output(RfwPin, GPIO.LOW)
        GPIO.output(RrvPin,GPIO.HIGH)
    else:
        GPIO.output(RfwPin, GPIO.LOW)
        GPIO.output(LrvPin,GPIO.LOW)
