import functools
import os
import random
import time
from piui import PiUi

current_dir = os.path.dirname(os.path.abspath(__file__))

l_status = 0
r_status = 0
status = object

class BoatPiUi(object):

    def __init__(self):
        self.title = None
        self.txt = None
        self.img = None
        self.ui = PiUi(img_dir=os.path.join(current_dir, 'imgs'))
        self.src = "sunset.png"

    def main_menu(self):
        global l_status
        global r_status
        global status 

        self.page = self.ui.new_ui_page(title="Boat control")
        self.page.add_element("hr")
        self.page.add_textbox("Current Engine Status", "p")
        self.page.add_textbox("LEFT | RIGHT", "p")
        status = self.page.add_textbox("L "+ str(l_status) + "  |  R " + str(r_status), "h2")

        self.page.add_element("hr")
        both_stop = self.page.add_button("Both Halt", self.onhaltclick)

        self.page.add_element("hr")
        self.page.add_textbox("Basic", "p")
        both_f_1 = self.page.add_button("LR FW 1", self.onf1click)
        both_f_2 = self.page.add_button("LR FW 2", self.onf2click)
        both_f_3 = self.page.add_button("LR FW 3", self.onf3click)
        self.page.add_element("br")
        both_r_1 = self.page.add_button("LR RV 1", self.onr1click)
        both_r_2 = self.page.add_button("LR RV 2", self.onr2click)
        both_r_3 = self.page.add_button("LR RV 3", self.onr3click)

        self.page.add_element("hr")
        self.page.add_textbox("Forward Control", "p")
        left_f_1 = self.page.add_button("L FW 1", self.onlf1click)
        right_f_1 = self.page.add_button("R FW 1", self.onrf1click)
        self.page.add_element('br')
        left_f_2 = self.page.add_button("L FW 2", self.onlf2click)
        right_f_2 = self.page.add_button("R FW 2", self.onrf2click)
        self.page.add_element('br')
        left_f_3 = self.page.add_button("L FW 3", self.onlf3click)
        right_f_3 = self.page.add_button("R FW 3", self.onrf3click)

        self.page.add_element('hr')
        self.page.add_textbox("Reverse Control", "p")
        left_r_1 = self.page.add_button("L RV 1", self.onlr1click)
        right_r_1 = self.page.add_button("R RV 1", self.onrr1click)
        self.page.add_element('br')
        left_r_2 = self.page.add_button("L RV 2", self.onlr2click)
        right_r_2 = self.page.add_button("R RV 2", self.onrr2click)
        self.page.add_element('br')
        left_r_3 = self.page.add_button("L RV 3", self.onlr3click)
        right_r_3 = self.page.add_button("R RV 3", self.onrr3click)
        self.page.add_element('hr')
        self.page.add_textbox("End of controls","p")
        self.page.add_element('hr')
                                         
    def onhaltclick(self):
        self.setengines(0,0)
    def onf1click(self):
        self.setengines(1,1)
    def onf2click(self):
        self.setengines(2,2)
    def onf3click(self):
        self.setengines(3,3)
    def onr1click(self):
        self.setengines(-1,-1)
    def onr2click(self):
        self.setengines(-2,-2)
    def onr3click(self):
        self.setengines(-3,-3)

    def onlf1click(self):
        self.setengines(1,9)
    def onrf1click(self):
        self.setengines(9,1)
    def onlf2click(self):
        self.setengines(2,9)
    def onrf2click(self):
        self.setengines(9,2)
    def onlf3click(self):
        self.setengines(3,9)
    def onrf3click(self):
        self.setengines(9,3)
                                         
    def onlr1click(self):
        self.setengines(-1,9)
    def onrr1click(self):
        self.setengines(9,-1)
    def onlr2click(self):
        self.setengines(-2,9)
    def onrr2click(self):
        self.setengines(9,-2)
    def onlr3click(self):
        self.setengines(-3,9)
        
    def onrr3click(self):
        self.setengines(9,-3)
        
    def setengines(self, l_value, r_value):
        global l_status
        global r_status
        global status
        if r_value != 9:
            r_status = r_value
        if l_value != 9:
            l_status = l_value
            
        # call set_engine(l_status,r_status)
        
        status.set_text("L "+ str(l_status) + "  |  R " + str(r_status))
            
    def main(self):
        self.main_menu()
        self.ui.done()
        
    
def main():
  piui = BoatPiUi()
  piui.main()

if __name__ == '__main__':
    main()
