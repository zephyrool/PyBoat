import functools
import os
import random
import time
from piui import PiUi
import RPi.GPIO as GPIO
from RPIO import PWM
current_dir = os.path.dirname(os.path.abspath(__file__))

# to get the onscreen status to update this is used as a global object
# there must be a better way to do this within the webpage, wihtin maain_menu or main
status = object

# stores the speeds of left and right
class engine_status():
    left = 0
    right = 0
    label = "L "+ str(left) + "  |  R " + str(right)

# stores the pin settings, sets up pins and sets up the DMA channels for PWM
# should consider whether to clear the gpio and dma channels at some point.
# also add a shutdown pi button?
class engine_config()
	GPIO.setmode(GPIO.BOARD)
	PWM.setup()
	RfwPin = 22
	RrvPin = 18
	RpwmPin = 16

	LfwPin = 15
	LrvPin = 13
	LpwmPin = 11 

	GPIO.setup(RfwPin,GPIO.OUT)
	GPIO.setup(RrvPin,GPIO.OUT)
	PWM.init_channel(0)

	GPIO.setup(LfwPin,GPIO.OUT)
	GPIO.setup(LrvPin,GPIO.OUT)
	PWM.init_channel(1)

# this will send the engine status to power the engines, using the info in the engine config, value 0 will est a dead stop, aso anything outside of the range +/-3. (the web buttons will only set the values to 3,2,1,0,-1,-2,-3, munis is backwards positive is forwards, and the magnitude scales the speed.
def power engine()
	if (engine_status.right > 3) or (engine_status.right == 0) or (engine_status.right < -3)
		GPIO.output(engine_config.RfwPin,GPIO.LOW)
		GPIO.output(engine_config.RrvPin,GPIO.LOW)
		PWM.add_channel_pulse(0,engine_config.Rpwmpin,0,0)
	elif engine_status.right > 0
		GPIO.output(engine_config.RfwPin,GPIO.HIGH)
		GPIO.output(engine_config.RrvPin,GPIO.LOW)
		PWM.add_channel+pulse(0,engine_config.RpwmPin,0,engine_status.right/3*1999)
	elif engine_status.right < 0
		GPIO.output(engine_config.RfwPin,GPIO.LOW)
		GPIO.output(engine_config.RrvPin,GPIO.HIGH)
		PWM.add_channel_pulse(0,engine_config.RpwmPin,o,-1*engine_status.right/3*1999)
		
  if (engine_status.left > 3) or (engine_status.left == 0) or (engine_status.left < -3)
		GPIO.output(engine_config.LfwPin,GPIO.LOW)
		GPIO.output(engine_config.LrvPin,GPIO.LOW)
		PWM.add_channel_pulse(0,engine_config.Rpwmpin,0,0)
	elif engine_status.left > 0
		GPIO.output(engine_config.LfwPin,GPIO.HIGH)
		GPIO.output(engine_config.LrvPin,GPIO.LOW)
		PWM.add_channel+pulse(0,engine_config.RpwmPin,0,engine_status.right/3*1999)
	elif engine_status.left < 0
		GPIO.output(engine_config.LfwPin,GPIO.LOW)
		GPIO.output(engine_config.LrvPin,GPIO.HIGH)
		PWM.add_channel_pulse(0,engine_config.RpwmPin,o,-1*engine_status.right/3*1999)

# this is the webpage based on the piuidemo
class BoatPiUi(object):

    def __init__(self):
        self.title = None
        self.txt = None
        self.img = None
        self.ui = PiUi(img_dir=os.path.join(current_dir, 'imgs'))
        self.src = "sunset.png"

    def main_menu(self):
        global status
        self.page = self.ui.new_ui_page(title="Boat control")
        self.page.add_element("hr")
        self.page.add_textbox("Current Engine Status", "p")
        self.page.add_textbox("LEFT | RIGHT", "p")
        status = self.page.add_textbox(engine_status.label)

        self.page.add_element("hr")
        both_stop = self.page.add_button("Both Halt", self.onhaltclick)

        self.page.add_element("hr")
        self.page.add_textbox("Basic", "p")
        both_f_1 = self.page.add_button("LR FW 1", self.onf1click)
        both_f_2 = self.page.add_button("LR FW 2", self.onf2click)
        both_f_3 = self.page.add_button("LR FW 3", self.onf3click)
        self.page.add_element("br")
        both_r_1 = self.page.add_button("LR RV 1", self.onr1click)
        both_r_2 = self.page.add_button("LR RV 2", self.onr2click)
        both_r_3 = self.page.add_button("LR RV 3", self.onr3click)

        self.page.add_element("hr")
        self.page.add_textbox("Forward Control", "p")
        left_f_1 = self.page.add_button("L FW 1", self.onlf1click)
        right_f_1 = self.page.add_button("R FW 1", self.onrf1click)
        self.page.add_element('br')
        left_f_2 = self.page.add_button("L FW 2", self.onlf2click)
        right_f_2 = self.page.add_button("R FW 2", self.onrf2click)
        self.page.add_element('br')
        left_f_3 = self.page.add_button("L FW 3", self.onlf3click)
        right_f_3 = self.page.add_button("R FW 3", self.onrf3click)

        self.page.add_element('hr')
        self.page.add_textbox("Reverse Control", "p")
        left_r_1 = self.page.add_button("L RV 1", self.onlr1click)
        right_r_1 = self.page.add_button("R RV 1", self.onrr1click)
        self.page.add_element('br')
        left_r_2 = self.page.add_button("L RV 2", self.onlr2click)
        right_r_2 = self.page.add_button("R RV 2", self.onrr2click)
        self.page.add_element('br')
        left_r_3 = self.page.add_button("L RV 3", self.onlr3click)
        right_r_3 = self.page.add_button("R RV 3", self.onrr3click)
        self.page.add_element('hr')
        self.page.add_textbox("End of controls","p")
        self.page.add_element('hr')
                                         
   # this will be used by each button, to change the status,  power the engines accordingly,and set the displayed status onscreen.
       def setengines(self, l_value, r_value):
        global status
        if r_value != 9:
            engine_status.right = r_value
        if l_value != 9:
            engine_status.left = l_value    
        power_engine()
        status.set_text(engine_status.label)
        
    # the variants for each button
    def onhaltclick(self):
        self.setengines(0,0)
    def onf1click(self):
        self.setengines(1,1)
    def onf2click(self):
        self.setengines(2,2)
    def onf3click(self):
        self.setengines(3,3)
    def onr1click(self):
        self.setengines(-1,-1)
    def onr2click(self):
        self.setengines(-2,-2)
    def onr3click(self):
        self.setengines(-3,-3)

    def onlf1click(self):
        self.setengines(1,9)
    def onrf1click(self):
        self.setengines(9,1)
    def onlf2click(self):
        self.setengines(2,9)
    def onrf2click(self):
        self.setengines(9,2)
    def onlf3click(self):
        self.setengines(3,9)
    def onrf3click(self):
        self.setengines(9,3)
                                         
    def onlr1click(self):
        self.setengines(-1,9)
    def onrr1click(self):
        self.setengines(9,-1)
    def onlr2click(self):
        self.setengines(-2,9)
    def onrr2click(self):
        self.setengines(9,-2)
    def onlr3click(self):
        self.setengines(-3,9)
        
    def onrr3click(self):
        self.setengines(9,-3)
    
    def main(self):
        self.main_menu()
        self.ui.done()
    
def main():
  piui = BoatPiUi()
  piui.main()

if __name__ == '__main__':
    main()
