#!/usr/bin/env python

# set speed of engines
# this will require a GPIO object, and setup of pwm channels o the "_engine"s, and setup of GPIO.OUT on direction pins (FW/RV_pin).

def set_engines(GPIO,L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine):
	# procedure to set the direction using the fw and rv pins
	def set_direction(speed, FW_pin, RV_pin):
		if speed == 0:
			GPIO.output(FW_pin,GPIO.LOW)
			GPIO.output(RV_pin,GPIO.LOW)
		elif speed < 0:
			GPIO.output(FW_pin,GPIO.LOW)
			GPIO.output(RV_pin,GPIO.HIGH)
		elif speed > 0 :
			GPIO.output(FW_pin,GPIO.HIGH)
			GPIO.output(RV_pin,GPIO.LOW)
		else :
			GPIO.output(FW_pin,GPIO.LOW)
			GPIO.output(RV_pin,GPIO.LOW)
			
	# procedure to set the speed (related to to the pwm duty cycle)
	def set_speed (engine, speed):
		if speed < 0 :
			speed = -speed
		if speed > 100:
			speed = 100
		engine.ChangeDutyCycle(speed)
	
	#now actually use those procedures withe the parameters recieved with the call to set engines going.
	set_direction(L_speed,L_FW_pin,L_RV_pin)
	set_direction(R_speed,R_FW_pin, R_RV_pin)
	set_speed(L_engine, L_speed)
	set_speed(R_engine, R_speed)
