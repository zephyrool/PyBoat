#!/usr/bin/env python

# import required modules
import time
import RPi.GPIO as GPIO
import cwiid
import functools
import os
import random
import sys
import LED
import engines

# use board pin numbers
GPIO.setmode(GPIO.BOARD)

# setup notification leds pins
Blue_pin = 13
Red_pin = 11
LEDFlashCount = 3
LEDFlashDelay = 0.1
GPIO.setup(Blue_pin,GPIO.OUT)
GPIO.setup(Red_pin,GPIO.OUT)

# set up wii remte parameters
button_delay = 0.1

#turn on red led as prompt to connect
GPIO.output(Red_pin,GPIO.HIGH)
print "Press 1 + 2 on your Wii Remote now ..."
time.sleep(1)
 
# Connect to the Wii Remote. If it times out
# flash red then quit. if it works flash blue and turn off the red
try:
  wii=cwiid.Wiimote()
except RuntimeError:
  print "Error opening wiimote connection"
  LED.flash(GPIO,Red_pin,LEDFlashCount,LEDFlashDelay)
  GPIO.cleanup()
  quit()
 
LED.flash(GPIO,Blue_pin,LEDFlashCount,LEDFlashDelay)
GPIO.output(Red_pin,GPIO.LOW)

print "Wii Remote connected...\n"
print "Press some buttons!\n"
print "Press PLUS and MINUS together to disconnect and quit.\n"

#set up wii to report buttons and NUNCHUCKs
wii.rpt_mode = cwiid.RPT_BTN |cwiid.RPT_NUNCHUK

# Setup engine control pins parameters
R_FW_pin = 29
R_RV_pin = 31
R_PW_pin = 32

L_FW_pin = 33
L_RV_pin = 37
L_PW_pin = 36 

# defaults to use for speed control
PW_frequency = 10
FW_speed_default = 60
RV_speed_default = -40
increment_speed_default = 10
speed_max = 100

#setup GPIO outputs
GPIO.setup(R_FW_pin,GPIO.OUT)
GPIO.setup(R_RV_pin,GPIO.OUT)
GPIO.setup(R_PW_pin,GPIO.OUT)

GPIO.setup(L_FW_pin,GPIO.OUT)
GPIO.setup(L_RV_pin,GPIO.OUT)
GPIO.setup(L_PW_pin,GPIO.OUT)


# set up engine PWM on the _PW pins
L_engine = GPIO.PWM(L_PW_pin,PW_frequency)
R_engine = GPIO.PWM(R_PW_pin,PW_frequency)
L_engine.start(0)
R_engine.start(0)

#start off with the engines at 0
L_speed = 0
R_speed = 0

while True:
	# create things that capture buttons from the wiimote and the nunchuk
	buttons = wii.state['buttons']
	nunchuk = wii.state['nunchuk']
	nunchuk_buttons = nunchuk['buttons']
 
	#If Plus and Minus buttons pressed together then rumble and quit.
	if (buttons - cwiid.BTN_PLUS - cwiid.BTN_MINUS == 0):
		print "\nClosing connection ..."
		wii.rumble = 1
		time.sleep(1)
		wii.rumble = 0
		LED.flash(GPIO,Red_pin,LEDFlashCount,LEDFlashDelay)
		GPIO.cleanup()
		exit(wii)
		sys.exit(0)

  # Check if other buttons are pressed by
  # doing a bitwise AND of the buttons number
  # and the predefined constant for that button.
	#button 1 for dead stop
	if (buttons & cwiid.BTN_1):
		L_speed = 0
		R_speed = 0
		engines.set_engines(GPIO,L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine)
		print "/n 1" 
		print "/n Lspeed " + str(L_speed) + " -  Rspeed " + str(R_speed) 
		time.sleep(button_delay)

	# dpad controls for basic controls
	if (buttons & cwiid.BTN_DOWN):
		L_speed = RV_speed_default
		R_speed = RV_speed_default
		engines.set_engines(GPIO,L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine)
		print "down /n" 
		print "/n Lspeed " + str(L_speed) + " -  Rspeed " + str(R_speed) 
		time.sleep(button_delay)
	if (buttons & cwiid.BTN_UP):
		L_speed = FW_speed_default
		R_speed = FW_speed_default
		engines.set_engines(GPIO,L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine)
		print "up /n" 
		print "/n Lspeed " + str(L_speed) + " -  Rspeed " + str(R_speed) 
		time.sleep(button_delay)
	if (buttons & cwiid.BTN_LEFT):
		L_speed = 0
		R_speed = FW_speed_default
		engines.set_engines(GPIO,L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine)
		print "left /n" 
		print "/n Lspeed " + str(L_speed) + " -  Rspeed " + str(R_speed) 
		time.sleep(button_delay)
	if (buttons & cwiid.BTN_RIGHT):
		L_speed = FW_speed_default
		R_speed = 0
		engines.set_engines(GPIO,L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine)
		print "right /n" 
		print "/n Lspeed " + str(L_speed) + " -  Rspeed " + str(R_speed) 
		time.sleep(button_delay)
		
	# trigger controls to increment and decrement speed on each side
	if (buttons & cwiid.BTN_B):
		R_speed = R_speed + increment_speed_default
		if R_speed >speed_max:
			R_speed = speed_max
		engines.set_engines(GPIO,L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine)
		print "B /n" 
		print "/n Lspeed " + str(L_speed) + " -  Rspeed " + str(R_speed) 
		time.sleep(button_delay)
	if (buttons & cwiid.BTN_A):
		R_speed = R_speed - increment_speed_default
		if R_speed < -speed_max:
			R_speed = -speed_max
		engines.set_engines(GPIO,L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine)
		print "A /n" 
		print "/n Lspeed " + str(L_speed) + " -  Rspeed " + str(R_speed) 
		time.sleep(button_delay)
	if (nunchuk_buttons & cwiid.NUNCHUK_BTN_Z):
		L_speed = L_speed + increment_speed_default
		if L_speed >speed_max:
			L_speed = speed_max
		engines.set_engines(GPIO,L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine)
		print "Z /n" 
		print "/n Lspeed " + str(L_speed) + " -  Rspeed " + str(R_speed) 
		time.sleep(button_delay)
	if (nunchuk_buttons & cwiid.NUNCHUK_BTN_C):
		L_speed = L_speed - increment_speed_default
		if L_speed < -speed_max:
			L_speed = -speed_max
		engines.set_engines(GPIO,L_speed, L_FW_pin, L_RV_pin, L_engine, R_speed, R_FW_pin, R_RV_pin, R_engine)
		print "C /n" 
		print "/n Lspeed " + str(L_speed) + " -  Rspeed " + str(R_speed) 
		time.sleep(button_delay)
