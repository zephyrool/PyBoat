#!/usr/bin/env python
# https://www.element14.com/community/docs/DOC-78055/l/adding-a-shutdown-button-to-the-raspberry-pi-b
# Simple script for shutting down the raspberry Pi at the press of a button.  
# by Inderpreet Singh  
  
import RPi.GPIO as GPIO  
import time  
import os  
import LED
 
# Use the Board Pin numbers  
# Setup the Pin with Internal pullups enabled and PIN in reading mode.  
GPIO.setmode(GPIO.BOARD)  
GPIO.setup(5, GPIO.IN, pull_up_down = GPIO.PUD_UP)  
Red_pin=11
GPIO.setup(Red_pin,GPIO.OUT)
# Our function on what to do when the button is pressed  
def Shutdown(self):  
    LED.flash(GPIO,11,5,0.25)
    os.system("sudo shutdown -h now")  
 
# Add our function to execute when the button pressed event happens  
GPIO.add_event_detect(5, GPIO.FALLING, callback = Shutdown, bouncetime = 1000)  
 
# Now wait!  
while 1:  
    time.sleep(1)  
