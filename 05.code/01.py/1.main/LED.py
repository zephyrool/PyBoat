#!/usr/bin/env python

# this is a simple routine to flash the led
# the pi GPIO needs to be setup as an object by the calling procedure and passed explicitly as the first argument
# pin - a GPIO pin connected to the anode, using an external current limiting resistor of a few K.
# LEdflashcount - the number of times to flash
# Ledflashdelay - the pause in seconds between flashes
import time
def flash(GPIO,pin,LEDFlashCount,LEDFlashDelay):
	for n in range(0, LEDFlashCount):
		GPIO.output(pin,GPIO.HIGH)
		time.sleep(LEDFlashDelay)
		GPIO.output(pin,GPIO.LOW)
		time.sleep(LEDFlashDelay)
